package fr.bonaparte.gsb.suividevosfrais;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

class AccesDistant {

    //private String urlSrvDev = " http://intranetgsb.pod3.sio.lan";
    //private String urlSrvProd = "http://10.10.2.165";
    //private String urlSrvDevAlex = "http://192.168.1.100";

    static void connexion(final Context context,final String infoConnexion, final VolleyCallback callback) {

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = "http://192.168.1.100/includes/android.php";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("connexion", "success :" + response);
                        callback.onSuccess(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("connexion", "erreur :" + error.getMessage());
            }
        }){
            // passage de parametres
        @Override
        protected Map<String, String> getParams() {
            Map<String, String> params = new HashMap<>();
            params.put("action", "connexion");
            params.put("infoConnexion", infoConnexion);
            Log.d("connexion", infoConnexion);
            return params;
        }
    };

// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    static void envoieDonnee(final Context context, final String idVisiteur, final String listeFrais, final VolleyCallback callback) {

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = "http://gsb.alexandre-petitjean.fr/includes/android.php";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("envoieDonnee", "success :" + response);
                        callback.onSuccess(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("envoieDonnee", "That didn't work!");
            }
        }){
            // passage de parametres
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("action", "envoieDonnee");
                params.put("idVisiteur", idVisiteur);
                params.put("listeFrais", listeFrais);
                Log.d("envoieDonnee", idVisiteur + " " + listeFrais);
                return params;
            }
        };

// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}

