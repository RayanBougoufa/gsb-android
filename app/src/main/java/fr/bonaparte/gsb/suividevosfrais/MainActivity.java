package fr.bonaparte.gsb.suividevosfrais;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    // Propriétés
    private EditText login;
    private EditText password;
    private Button btnValider;

    /**
     * Methode onCreate
     * Valorise les propriétés.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.login = findViewById(R.id.champ_login);
        this.password = findViewById(R.id.champ_password);
        this.btnValider = findViewById(R.id.button_valider);

        this.login.setText("lvillachane");
        this.password.setText("jux7g");

        String infoConnexion = this.login.getText().toString() + ":" + this.password.getText().toString();
        connexion(this, infoConnexion );
    }

    /**
     * Methode qui catch le clic sur le bouton de connexion.
     */
    private void connexion(final Context context, final String infoConnexion) {
        this.btnValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AccesDistant.connexion(context , infoConnexion , new VolleyCallback() {
                    @Override
                    public void onSuccess(String result) {
                        String[] resultat = result.split(":");
                        if (resultat[0].equals("ok")){
                            Global.idVisiteur = resultat[1];
                            Intent intent = new Intent( MainActivity.this, MenuActivity.class );
                            startActivity(intent);
                        }else{
                            Toast.makeText(context, "Erreur de login/mot de passe", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
    }
}
