package fr.bonaparte.gsb.suividevosfrais;

public interface VolleyCallback {
    void onSuccess(String result);
}
